# modules/ec2_instances/main.tf
resource "aws_instance" "this" {
  count         = var.instance_count
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id

  tags = {
    Name = "${var.instance_name_prefix}-${count.index + 1}"
  }
}
