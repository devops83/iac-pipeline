# modules/ec2_instances/variables.tf
variable "instance_count" {
  description = "Number of EC2 instances to create"
  type        = number
}

variable "ami_id" {
  description = "ID of the Amazon Machine Image (AMI) for the EC2 instances"
  type        = string
}

variable "instance_type" {
  description = "Instance type for the EC2 instances"
  type        = string
}

variable "subnet_id" {
  description = "ID of the subnet where the EC2 instances will be launched"
  type        = string
}

variable "instance_name_prefix" {
  description = "Prefix for the name tag of the EC2 instances"
  type        = string
}
