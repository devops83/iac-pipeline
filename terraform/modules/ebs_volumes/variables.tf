# modules/ebs_volumes/variables.tf
variable "volume_count" {
  description = "Number of EBS volumes to create"
  type        = number
}

variable "availability_zone" {
  description = "Availability zone for the EBS volumes"
  type        = string
}

variable "volume_size" {
  description = "Size of the EBS volumes (in GB)"
  type        = number
}

variable "volume_name_prefix" {
  description = "Prefix for the name tag of the EBS volumes"
  type        = string
}

variable "instance_ids" {
  description = "List of EC2 instance IDs to attach the EBS volumes"
  type        = list(string)
}

variable "volume_device_name" {
  description = "List of device names to attach the EBS volumes to the instances"
  type        = list(string)
}
