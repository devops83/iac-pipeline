# modules/ebs_volumes/main.tf
resource "aws_ebs_volume" "this" {
  count             = var.volume_count
  availability_zone = var.availability_zone
  size              = var.volume_size
  tags = {
    Name = "${var.volume_name_prefix}-${count.index + 1}"
  }
}

resource "aws_volume_attachment" "this" {
  count       = var.volume_count
  device_name = var.volume_device_name[count.index]
  volume_id   = aws_ebs_volume.this[count.index].id
  instance_id = var.instance_ids[count.index]
}
