
# Load the appropriate variable file based on the environment
terraform {
  required_version = ">= 1.0.0"
}

locals {
  environment = "dev"   # Change this to "prod" when deploying to the production environment
}

# Load the variable file based on the environment
terraform {
  required_version = ">= 1.0.0"
}

locals {
  environment = "dev"   # Change this to "prod" when deploying to the production environment
}

module "s3_bucket" {
  source      = "./modules/s3"
  bucket_name = var.bucket_name
}

module "vpc" {
  source     = "./modules/vpc"
  vpc_cidr   = var.vpc_cidr
  vpc_name   = var.vpc_name
}

module "subnet" {
  source           = "./modules/subnet"
  vpc_id           = module.vpc.vpc_id
  subnet_cidr      = var.subnet_cidr
  availability_zone = var.availability_zone
  subnet_name      = var.subnet_name
}

module "ec2_instances" {
  source           = "./modules/ec2_instances"
  instance_count   = var.instance_count
  ami_id           = var.ami_id
  instance_type    = var.instance_type
  subnet_id        = module.subnet.subnet_id
  instance_name_prefix = var.instance_name_prefix
}

module "ebs_volumes" {
  source              = "./modules/ebs_volumes"
  volume_count        = var.volume_count
  availability_zone   = var.availability_zone
  volume_size         = var.volume_size
  volume_name_prefix  = var.volume_name_prefix
  instance_ids        = module.ec2_instances.instance_ids
  volume_device_name  = var.volume_device_name
}
