# Use an official, minimal base image of Ubuntu 22.04
FROM ubuntu:22.04

# Create a non-root user and switch to it
RUN groupadd -g 1000 automation \
    && useradd -u 1000 -g automation -s /bin/bash -m automation

# Switch to the non-root user
USER automation

# Install required packages
RUN sudo apt-get update \
    && sudo apt-get install -y --no-install-recommends \
        curl iproute2 sshfs unzip less groff \
    && sudo rm -rf /var/lib/apt/lists/*

# Install kubectl
RUN curl -LO https://dl.k8s.io/release/v1.23.6/bin/linux/amd64/kubectl \
    && sudo install -o automation -g automation -m 0755 kubectl /usr/local/bin/kubectl \
    && rm kubectl

# Install AWS CLI
RUN curl -LO https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip \
    && unzip awscli-exe-linux-x86_64.zip \
    && sudo ./aws/install \
    && rm -rf awscli-exe-linux-x86_64.zip ./aws

# Add the AWS CLI installation directory to the PATH
ENV PATH="/usr/local/bin:${PATH}"

# Optional: Set a working directory inside the container
 WORKDIR /app

# Optional: Copy application files to the container 
 COPY . /app

# Optional: Specify the command to run when the container starts 
# CMD ["python", "app.py"]
