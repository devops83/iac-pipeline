This is the structure of the overall IaaC code
iac-pipeline/
├── .gitlab-ci.yml
├── terraform/
│   ├── main.tf
│   ├── variables.tf
│   ├── outputs.tf
│   ├── terraform.tfvars
│   └── modules/
│       └── s3/
│           ├── main.tf
│           └── variables.tf
│       └── vpc/
│           ├── main.tf
│           └── variables.tf
│       └── subnet/
│           ├── main.tf
│           └── variables.tf
├── tests/
│   ├── integration/
│   │   └── integration_test.sh
│   └── security/
│       └── security_test.sh
├── scripts/
│   ├── init.sh
│   ├── validate.sh
│   └── deploy.sh
└── .gitignore

